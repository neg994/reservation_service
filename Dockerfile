FROM openjdk:8-jdk-alpine
EXPOSE 8084
ARG JAR_FILE=target/reservation-service-0.0.1-SNAPSHOT.war
ADD ${JAR_FILE} app.war
ENTRYPOINT ["java","-jar","/app.war"]